const express = require('express');
const mongoose = require('mongoose');
const jsonParser = require('body-parser').json();

const PORT = process.env.PORT || 3000;

const MONGO_URI = process.env.MONGO_URI || 'mongodb://samheutmaker:ryleeisgay69@apollo.modulusmongo.net:27017/baSa5riv';

var app = express();

app.use(express.static(__dirname + '/www'));

// Another random comment 3
mongoose.connect(MONGO_URI);

var authRoutes = require(__dirname + '/routes/auth-routes');
var productRoutes = require(__dirname + '/routes/product-routes');
var userRoutes = require(__dirname + '/routes/user-routes');

app.use('/auth', authRoutes);
app.use('/api/products', productRoutes);
app.use('/api/user', userRoutes);

app.listen(PORT, () => {
  console.log('Server up on port ' + PORT);
});
