'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function() {
  gulp.src('./www/styles/scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./www/styles/css'));
});

gulp.task('default', function() {
  gulp.watch('./www/styles/scss/*.scss', ['sass']);
});
