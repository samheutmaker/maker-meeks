const dbError = require(__dirname + '/db-error');
const dbNoData = require(__dirname + '/db-no-data');

module.exports = exports = (res) => {
  return (err, data) => {
    // Check for dbError
    if(err) dbError(err, res);
    if(!data) dbNoData(data, res);

    res.status(200).json({
      msg: 'Success',
      data: data
    });

  }
}
