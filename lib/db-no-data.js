// Handles a database error
module.exports = exports = function(data, res) {
  return res.status(500).json({
    msg: 'Null Data Value',
    data: data
  });
}
