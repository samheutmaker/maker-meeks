// Handles a database error
module.exports = exports = function(err, res) {
  return res.status(500).json({
    msg: 'Error retreiving data',
    error: err
  });
}
