angular.module('AuthController', [])

// Auth Controller
.controller('AuthController', function($scope, Auth, $window, $location) {

  $scope.changeView = {
    register: function() {
      this.views.register = true;
      this.views.login = false;
    },
    login: function() {
      this.views.register = false;
      this.views.login = true;
    },
    views: {
      login: true,
      register: false
    }
  };


})

// Regsiter Controller
.controller('RegisterController', function($scope, Auth, $window, $location, $rootScope) {

  $scope.register = {
    email: '',
    password: '',
    checkPassword: ''
  };

  $scope.registerUser = function() {
    // Check to make sure all feilds are filled out
    if ($scope.register.email.length && $scope.register.password.length && $scope.register.passwordCheck.length) {
      // Check to make sure the password is confirmed
      if ($scope.register.password === $scope.register.passwordCheck) {
        //Register User
        Auth.register($scope.register).then(function(res) {
          // If the token exists
          if (res.data.token) {
            // Store the token as a sessio variable
            $window.sessionStorage.token = res.data.token;
            // Redirect user to product page
            $location.url('/');
          }
        }, function(err) {
          console.log('Error');
        });
        // Passwords do not match
      } else {
        $rootScope.auth.msg = 'Passwords do not match.';
      }
      // Fields are not filled out
    } else {
      $rootScope.auth.msg = 'Please fill out all fields';
    }
  }

})

// Login Controller
.controller('LoginController', function($scope, Auth, $window, $location) {

  $scope.loginUser = function() {

    $scope.authString = $scope.login.email + ':' + $scope.login.password;

    Auth.login(btoa($scope.authString)).then(function(res) {
      // if the token exists, store it in session storage and redirect to account page
      if (res.data.token) {
        // Store token
        $window.sessionStorage.token = res.data.token;
        // Redirect user
        $location.url('/');
      }
    });
  }
})