angular.module('ProfileController', [])
// Nav Controller
.controller('AdminController', function($scope, $location, $timeout, $window, Products) {
  // All Items
  $scope.adminItems = [];

  // Get All Item
  $scope.getItems = function() {
    Products.getAll().then(function(res) {
      if (res) {
        $scope.adminItems = res.data.data;
      }
    }).then(function(err) {
      console.log(err);
    });
  }

  $scope.getItems();


  // Select Item for admin control
  $scope.selectItemAdmin = function(id) {
    $scope.selectedItemId = id;
  }
  // Go to Detail view
  $scope.goToItem = function(id) {
    var path = '/detail/' + id;
    $location.url(path);
  }

  // Permanently delete Item and reload items
  $scope.removeItem = function() {
    Products.removeOne($scope.selectedItemId).then(function(data) {
      console.log(data);
      $scope.getItems();
    })
    $scope.showWarning = false;
  }
  //Toggle new Item interface
  $scope.showNewItemInterface = false;
  $scope.toggleNewItemInterface = function() {
    $scope.showNewItemInterface = !$scope.showNewItemInterface;
    console.log('Clicked');
  }


  // Create New Item and reload items
  $scope.createNewItem = function(item){
    console.log('hit')
    Products.addOne(item).then(function(){
      $scope.toggleNewItemInterface();
      $scope.getItems();
      item = null;
    })
  };


})