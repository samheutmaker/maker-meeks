angular.module('CheckoutController', [])

.controller('CheckoutController', ['$scope', 'User',
  function($scope, User) {


  }
])
  .directive('userCart', function() {
    return {
      restrict: 'AEC',
      replace: true,
      templateUrl: 'app/templates/checkout/cart.html',
      scope: {},
      controller: function($scope, User) {

        // Vars
        $scope.user = {};
        $scope.cart = [];

        // Get user obj
        $scope.$watch(function() {
          return User.user;
        }, function() {
          $scope.user = User.user;
        });
        // Get cart
        $scope.$watch(function() {
          return User.cart;
        }, function() {
          $scope.cart = User.cart;
        });
      }
    }
  })

.directive('userShipping', function() {
  return {
    restrict: 'AEC',
    replace: true,
    templateUrl: 'app/templates/checkout/shipping.html',
    scope: {
      user: '=user'
    },
    link: function(scope, el, attr) {
    	// Update User
    	scope.$watch(function() {
    		return scope.user
    	}, function() {
    		console.log(scope.user);
    	});
    },
    controller: function($scope, User, $interval, Address) {

    	$scope.state = {
    		addAddress: false
    	};

      $scope.removeAddress = function(address){
        var toRemove = User.user.shipping.splice(User.user.shipping.indexOf(address), 1)[0]._id;
        Address.removeAddress(toRemove).then(function(res){
          console.log(res);
        })
      }

      $scope.addAddress = function() {
        Address.addAddress($scope.newAddress)
        .then(function(res) {
          console.log(res);
        });
      }

    }
  }
})