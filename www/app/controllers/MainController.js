angular.module('MainController', [])
// Nav Controller
.controller('NavController', function($scope, $location, $timeout, $window) {
  // Track menu open
  $scope.menu = {
    isOpen: false,
    toggle: function() {
      this.isOpen = !this.isOpen;
    }
  };
  $scope.$watch(function() {
    return $window.sessionStorage.token;
  }, function() {
    $scope.changeView();
  })
  $scope.changeView = function() {
    if ($window.sessionStorage.token) {
      $scope.include.url = '/app/templates/account/account.html';
      $scope.auth = false;
    } else {
      $scope.include.url = '/app/templates/auth/auth.html'
      $scope.auth = true;
    }
  };
  $scope.include = {
    url: '/app/templates/auth/auth.html'
  };
  // Close menu on location change
  $scope.$watch(function() {
    return $location.path()
  }, function() {
    $scope.menu.isOpen = false;
  });
})
// Home Controller
.controller('HomeController', function($scope, Products) {
  $scope.products = {};
  Products.getAll().then(function(res) {
    $scope.formatProducts(res.data.data);
  });
  $scope.formatProducts = function(products) {
    var newProducts = [];
    for (var i = 0; i < products.length; i = i + 2) {
      if (products[i + 1]) {
        var row = [products[i], products[i + 1]];
      } else {
        var row = [products[i]];
      }
      newProducts.push(row);
    }
    $scope.products = newProducts;
    console.log(newProducts);
  }
})
  .controller('SlideshowContainer', ['$scope',
    function($scope) {
      console.log('nothine');
    }
  ])
// Detail Controller
.controller('DetailController', function($scope, $routeParams, $rootScope,
  User, Products) {
  // Blank object that will hold selected product
  $scope.detailItem = {};
  $scope.changeSize = function(size) {}
  $scope.detailItem = {
    size: 'L',
    changeSize: function(size) {
      this.size = size;
    }
  };
  // Get product for detail view
  Products.getOne($routeParams.id).then(function(res) {
    $scope.detailItem.item = res.data.data;
  })
  $scope.addToCart = function() {
    //Add to Cart
    User.addToCart($scope.detailItem.item._id, 2, $scope.detailItem.size);
  }
})
// Cart Controller
.controller('AccountController', function($scope, User, $location) {
  $scope.cart = {};
  $scope.removeAllItems = function() {
    User.addToCart().then(function(data) {
      console.log(data);
    })
  }
  // Retrieve user cart
  $scope.loadCart = function() {
    User.getCart().then(function(res) {
      User.cart = res.data.cart;
      $scope.cart = User.cart;
    })
  };
  $scope.goToDetail = function(id) {
    var path = '/detail/' + id;
    $location.url(path);
  }
  // Load cart initially
  $scope.loadCart();
  // Load cart on cart change
  $scope.$on('CARTCHANGED', function(event, args) {
    $scope.loadCart();
    console.log('Cart Changed');
  });
})
// Contact Controller
.controller('ContactController', function($scope) {
  console.log('contact page');
})