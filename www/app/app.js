angular.module('notify', ['ngRoute', 'MainController', 'MainService',
  'AuthController', 'ngAnimate', 'ProfileController', 'CheckoutController'
])


.config(['$routeProvider', '$locationProvider',
  function($routeProvider,
    $locationProvider) {

    $routeProvider

    .when('/', {
      templateUrl: '/app/templates/products/products.html',
      controller: 'HomeController'
    })
      .when('/checkout', {
        templateUrl: '/app/templates/checkout/checkout.html',
        controller: 'CheckoutController'
      })
      .when('/detail/:id', {
        templateUrl: '/app/templates/detail/detail.html',
        controller: 'DetailController'
      })
      .when('/contact', {
        templateUrl: '/app/templates/contact/contact.html',
        controller: 'ContactController'
      })
      .when('/admin', {
        templateUrl: '/app/templates/admin/admin.html',
        controller: 'AdminController'
      })

    .otherwise({
      redirectTo: '/'
    });

    $locationProvider.hashPrefix('!');


  }
])

.config(function($httpProvider) {
  $httpProvider.interceptors.push('authInterceptor');
})
  .run(function($rootScope, $location, $window, User) {

    $rootScope.$on('$routeChangeStart', function(event, next) {

      // Check if the user is logged in
      var userAuthenticated = $window.sessionStorage.token;


      // Load User
      if (userAuthenticated && User.user === null) {
        User.getUser().then(function(res) {
          User.user = res.data.user;
        }, function(err) {
          User.user = null;
        });
      }

      // check if user is authenticated and trying to access secure page
      if (userAuthenticated && next.isLogin) {
        /* You can save the user's location to take him back to the same page after he has logged-in */
        // $rootScope.savedLocation = $location.url();
        $location.path(BuildPathFromRoute(next));
      }
      // check if user is not authenticated and trying to access secure page
      else if (!userAuthenticated && next.isLogin) {
        $location.url('/register');
      }
      // user not authenticated and trying to access insecure page
      else {
        $location.path(BuildPathFromRoute(next));
      }
    });
  })

  .run(['$rootScope', '$location', function($rootScope, $location) {

    $rootScope.$on('$routeChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
      var path = $location.path();
      var queryString = '';
      var referrer = '';


      if(path.indexOf('?') !== -1) {
        queryString = path.substring(path.indexOf('?'), path.length);
      }

      // if (fromState.name) {
      //   referrer = $location.protocol() + '://' + $location.host() + '/!#' + fromState.url;
      // }

      analytics.page({
        path: path,
        referrer: referrer,
        search: queryString,
        url: $location.url()
      });
    });

  }])


// Builds a route from the parameters
function BuildPathFromRoute(routeObj) {
  var path = routeObj.$$route.originalPath;
  for (var property in routeObj.pathParams) {
    if (routeObj.pathParams.hasOwnProperty(property)) {
      var regEx = new RegExp(":" + property, "gi");
      path = path.replace(regEx, routeObj.pathParams[property].toString());
    }
  }
  return path;
}