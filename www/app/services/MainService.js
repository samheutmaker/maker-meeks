angular.module('MainService', [])

// Adds auth to all requests
.factory('authInterceptor', function($rootScope, $q, $window) {
  return {
    request: function(req) {
      req.headers = req.headers || {};
      if ($window.sessionStorage.token) {
        // retrieve token from session storage if it exists; store in config object
        req.headers.token = $window.sessionStorage.token;
      }
      return req;
    },
    response: function(response) {
      if (response.status === 401) {
        // handle the case where the user is not authenticated
      }
      return response || $q.when(response);
    }
  };
})

.factory('Address', ['$http', function($http){

    return {
      addAddress: function(address){
        return $http.post('/api/user/address/new', address);
      },
      removeAddress: function(address_id){
        console.log('jot');
        return $http.delete('/api/user/address/' + address_id);
      }
    }
}])

// Handles user creation and token generation
.service('Auth', ['$http',
  function($http) {
    // Login
    this.login = function(headerData) {
      return $http({
        methods: 'GET',
        url: '/auth/login',
        headers: {
          authorization: 'Basic ' + headerData
        }
      });
    };
    // Register
    this.register = function(data) {
      return $http.post('/auth/register', data);
    };
  }
])

// Handles adding, deleting and retrieving products
.service('Products', ['$http',
  function($http) {
    // Base path for products
    this.basePath = '/api/products';
    // Get all Products
    this.getAll = function() {
      return $http.get(this.basePath);
    };
    // Get one Product based on Id
    this.getOne = function(id) {
      var URI = this.basePath + '/' + id;
      return $http.get(URI);
    };
    // Add new product
    this.addOne = function(item) {
      var URI = this.basePath + '/new';
      return $http.post(URI, item);
    }
    // Remove Product
    this.removeOne = function(id) {
      var URI = this.basePath + '/delete/' + id;
      return $http.delete(URI);
    }
  }
])

.factory('BroadcastService', function($rootScope) {
  return {
    emit: function(msg, data) {
      $rootScope.$broadcast(msg, data);
    }
  }
})

// Handles all user interaction besides Auth
.factory('User', ['$http', '$rootScope',
  function($http, $rootScope) {

    return {
      user: null,
      getUser: function() {
        return $http.get(this.basePath);
      },
      // Base path for all user routes
      basePath: '/api/user',

      // Get user cart
      getCart: function() {
        var URI = this.basePath + '/cart';
        return $http.get(URI);
      },
      // User Cart
      cart: [],
      //add item to cart
      updateCart: function() {
        var URI = this.basePath + '/cart/add';
        return $http.post(URI, this.cart);
      },
      addToCart: function(product_id, quantity, size) {

        if (product_id !== null && quantity !== null && size !== null) {
          // Tracks if we found a match in our cart
          var matched = true;
          if (this.cart.length > 0) {
            this.cart.forEach(function(currentItem, itemIndex) {
              // If we find an item with the same id and size
              if (currentItem.product_id == product_id &&
                currentItem.size == size) {
                // Update the quantity of this item
                currentItem.quantity = quantity;
                // Track this
                matched = false;
              }
            });
          }
          if (matched) {
            this.cart.push({
              product_id: product_id,
              size: size,
              quantity: quantity
            });
          }
          this.updateCart().then(function(
              updateCartRes) {
              $rootScope.$broadcast('CARTCHANGED', null);
            },
            function(err) {
              console.log('Failed to add to cart');
            });
        }
      }
    }
  }
]);